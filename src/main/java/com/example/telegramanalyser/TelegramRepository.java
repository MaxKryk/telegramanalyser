package com.example.telegramanalyser;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TelegramRepository extends JpaRepository<Telegram, Long> {
}
