package com.example.telegramanalyser;

import lombok.Data;

@Data
public class TelegramTESTZCCA extends Telegram {
    private String whitespaces;
    private String name;
private String version;
private String timestamp;
private String referenceNumber;
private String filler;
private String LocationId;

private final String shouldHaveWhitespaces = " 8 whitespaces";
    private final String shouldHaveName = " 8 CHARS";
    private final String shouldHaveVersion = " 4 INT";
    private final String shouldHaveTimestamp = " 19 CHARS";
    private final String shouldHaveReferenceNumber = "12 INT";
    private final String shouldHaveFiller = " 7 INT";
    private final String shouldHaveLocationId = " 4 INT";

}
