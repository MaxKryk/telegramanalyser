package com.example.telegramanalyser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@Slf4j

public class TelegramController {

private final TelegramRepository repository;

Validation validation=new Validation();
TelegramController(TelegramRepository repository){this.repository=repository;}

    @GetMapping("/main_page")
    public ModelAndView getMainPage(Model model){
        return new ModelAndView( "main_page" );}

@GetMapping("/enter_telegram")
    public ModelAndView getEnterTelegram(Model model){
    model.addAttribute( "telegram", new Telegram() );
    return new ModelAndView( "enter_telegram" );
}
@PostMapping("/enter_telegram")
    public ModelAndView postEnterTelegram(Model model, @ModelAttribute Telegram telegram) {
    repository.save( telegram );
    StringBuilder stringBuilder = new StringBuilder( telegram.getAllCharactersString() );
    String telegramName = stringBuilder.substring( 8, 16 );
    String whitespaces=stringBuilder.substring( 0,8 );
    telegram.setWhitespaces( stringBuilder.substring( 0,8 ));
    //.addAttribute( "whitespaces", whitespaces );
    telegram.setName( telegramName );

    System.out.println( telegram.getName() );
    if (validation.whiteSpaceValidation( whitespaces.toCharArray() )!=true){
          return new ModelAndView( "whitespaces_dont_match" );
       }else{
    if (telegram.getName().equals( "TESTZCCA" )) {
        TelegramTESTZCCA telegramTESTZCCA = new TelegramTESTZCCA();
        String telegramContent = telegram.getAllCharactersString();
        telegramTESTZCCA.setWhitespaces( telegramContent.substring( 0, 8 ) );
        telegramTESTZCCA.setName( telegramContent.substring( 8, 16 ) );
        telegramTESTZCCA.setVersion( telegramContent.substring( 16, 20 ) );
        telegramTESTZCCA.setTimestamp( telegramContent.substring( 20, 39 ) );
        telegramTESTZCCA.setReferenceNumber( telegramContent.substring( 39, 51 ) );
        telegramTESTZCCA.setFiller( telegramContent.substring( 51, 58 ) );
        telegramTESTZCCA.setLocationId( telegramContent.substring( 58, 62 ) );
        model.addAttribute( "telegram_testzcca", telegramTESTZCCA );
        return new ModelAndView( "enter_telegram_TESTZCCA_success" );
    } else {
        System.out.println( "Unknown telegram type" );
        return new ModelAndView( "unknown_telegram_type" );
    }
}
}}





