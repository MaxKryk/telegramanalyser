package com.example.telegramanalyser;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class Telegram {
    private @Id
    @GeneratedValue
    Long id;
    private String allCharactersString;
    private String name;
    private String whitespaces;
}